<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Base Controller:
Route::post('base/registration', ['as' => 'register', 'uses' => 'Api\baseController@postRegister'])->middleware('noLogin');
Route::post('base/login', ['as' => 'login', 'uses' => 'Api\baseController@postLogin'])->middleware('noLogin');
Route::get('base/logout', ['as' => 'logout', 'uses' => 'Api\baseController@getLogout'])->middleware('Login');
Route::get('base/user/{id}', ['as' => 'logout', 'uses' => 'Api\baseController@getUser']);
Route::get('base/user-info-login', ['as' => 'userinfologin', 'uses' => 'Api\baseController@checkUserLogin'])->middleware('Login');


