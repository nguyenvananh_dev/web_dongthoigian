<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mixer extends Model
{
    public $tablename = 'mixers';
}
