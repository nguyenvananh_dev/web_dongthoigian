<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTag extends Model
{
    public $tablename = 'bai_dang';

    public function User()
    {
        return beLongsTo('App\User', 'user_id');
    }

    public function DongThoiGian()
    {
        return beLongsTo('App\DongThoiGian', 'dongthoigian_id');
    }

    public function SuKien()
    {
        return beLongsTo('App\SuKien', 'sukien_id');
    }
    public function BaiDang()
    {
        return beLongsTo('App\BaiDang', 'baidang_id');
    }
}
