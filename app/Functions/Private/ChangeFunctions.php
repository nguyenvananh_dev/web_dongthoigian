<?php
    function ChangecomeBackALL(&$val)
    {
        $result = $val->all();
        foreach ($result as $index => $value) {
            $val[$index] = ChangecomeBack($value);
        }
        return true;
    }
    function ChangecomeBack($val)
    {
        return base64_decode(base64_decode(base64_decode($val)));
    }
    # Function Change Username
    function changeUsername($val)
    {
        return data_encode(data_encode($val));
    }

    function rechangeUsername($val)
    {
        return data_decode(data_decode($val));
    }

    # Function Change Number phone
    function changeNumberphone($phone)
    {
        if($phone == null){
            return $phone;
        }
        $in_phone = $phone;
        $out_phone = [];
        $out_phone[0] = null;
        $out_phone[1] = null;
        if (isNumberphone($in_phone)){
            $in_phone = strtolower($in_phone);
            for ($i=0; $i < (strlen($in_phone) - 1) ; $i += 2) {
                $tg = $in_phone[$i + 1];
                $in_phone[$i + 1] = $in_phone[$i];
                $in_phone[$i] = $tg;
                $out_phone[0] = $out_phone[0] . $in_phone[$i];
                $out_phone[0] = $out_phone[0] . $in_phone[$i + 1];
                $out_phone[1] = $in_phone[$i] . $out_phone[1];
            }
            for ($i=0; $i < (strlen($in_phone) - 1) ; $i += 2) {
                $tg = $in_phone[$i + 1];
                $in_phone[$i + 1] = $in_phone[$i];
                $in_phone[$i] = $tg;
            }
            #Bước cuối:
            $out_phone = null;
            for ($i=0; $i < (strlen($in_phone)) ; $i ++) {
                $out_phone[] = ord($in_phone[$i]);
            }
            $in_phone = implode('0_0', $out_phone);
            for ($i=0; $i < (strlen($in_phone) - 1) ; $i += 2) {
                $tg = $in_phone[$i + 1];
                $in_phone[$i + 1] = $in_phone[$i];
                $in_phone[$i] = $tg;
            }
            return $in_phone;
        }else{
            return false;
        }
    }
    function rechangeNumberphone($phone)
    {
        if($phone == null){
            return $phone;
        }
        $in_phone = $phone;
        for ($i=0; $i < (strlen($in_phone) - 1) ; $i += 2) {
            # Thay đổi và đảo vị trí:
            $tg = $in_phone[$i + 1];
            $in_phone[$i + 1] = $in_phone[$i];
            $in_phone[$i] = $tg;
        }
        $in_phone = explode('0_0', $in_phone);
        $rq = null;
        foreach ($in_phone as $value) {
            $rq = $rq. chr($value);
        }
        $in_phone = $rq;
        return $in_phone;
    }

    #Function Change Date
    function changeDate($e)
    {
		if($e == null){
			return null;
		}
		# Định dạng truyền vào là: Y-M-D:
		$e = explode('-', $e);
		# Nếu là năm lẻ thì cộng vào 3 năm còn nếu là năm chẵn thì cộng vào 5 năm:
		if($e[0] % 2 == 0){
			$e[0] = $e[0] + 5;
		}else{
			$e[0] = $e[0] + 3;
		}
		# Nếu là tháng lẻ thì cộng thêm 1 nếu là tháng chẵn thì trừ đi 1
		if($e[1] % 2 == 0){
			$e[1] = $e[1] - 1;
		}else{
			$e[1] = $e[1] + 1;
		}
		# Nếu ngày chẵn thì trừ 1 nếu ngày lẻ thì cộng 1
		if($e[2] % 2 == 0){
			$e[2] = $e[2] - 1;
		}else{
			$e[2] = $e[2] + 1;
		}
		return implode('-', $e);
    }
    function rechangeDate($e)
    {
		if($e == null){
			return null;
		}
		# Định dạng truyền vào là: Y-M-D:
		$e = explode('-', $e);

		# Nếu ngày chẵn thì trừ 1 nếu ngày lẻ thì cộng 1
		if($e[2] % 2 == 0){
			$e[2] = $e[2] - 1;
		}else{
			$e[2] = $e[2] + 1;
		}
		# Nếu là tháng lẻ thì cộng thêm 1 nếu là tháng chẵn thì trừ đi 1
		if($e[1] % 2 == 0){
			$e[1] = $e[1] - 1;
		}else{
			$e[1] = $e[1] + 1;
		}
		# Nếu là năm lẻ thì trừ vào 5 năm còn nếu là năm chẵn thì trừ vào 3 năm:
		if($e[0] % 2 == 0){
			$e[0] = $e[0] - 3;
		}else{
			$e[0] = $e[0] - 5;
		}
		return implode('-', $e);
    }

    # Function Change Email
    function changeEmail($val)
    {
		if($val == null){
			return null;
		}
        $val = explode('@', $val);
        if(count($val) == 2)
        {
            $val[0] = data_encode($val[0]);
            $val[1] = data_encode($val[1]);
            return implode('@', $val);
        }
        return null;
    }
    function rechangeEmail($val)
    {
		if($val == null){
			return null;
		}
		$val = explode('@', $val);
		$val[0] = data_decode($val[0]);
		$val[1] = data_decode($val[1]);
		return implode('@', $val);
    }
// ============================================================================ //
    # Các hàm xử lý phụ cho 4 hàm chính trên
	function data_encode($val)
	{
		if($val == null){
			return null;
		}
		$val = base64_encode($val);

		for ($i=0; $i < (strlen($val) - 1) ; $i += 2) {
			# Thay đổi và đảo vị trí:
			$tg = $val[$i + 1];
			$val[$i + 1] = $val[$i];
			$val[$i] = $tg;
		}

		$val = base64_encode($val) . 'a';

		return $val;
	}
	function data_decode($val)
	{
		if($val == null){
			return null;
		}
		$val = mb_substr($val, 0, mb_strlen($val) - 1);
		$val = base64_decode($val);

		for ($i=0; $i < (strlen($val) - 1) ; $i += 2) {
			# Thay đổi và đảo vị trí:
			$tg = $val[$i + 1];
			$val[$i + 1] = $val[$i];
			$val[$i] = $tg;
		}

		$val = base64_decode($val);

		return $val;
	}
    function isNumberphone($phone)
    {
        if($phone == null){
            return $phone;
        }
        $pattern = '/^(84|0)[0-9]{9}/m';
        preg_match_all($pattern, $phone, $matches, PREG_SET_ORDER, 0);
        $check = null;
        foreach ($matches as $value) {
            $check = $check . $value[0];
        }
        if ($phone == $check){
            return true;
        }else{
            return false;
        }
    }
?>
