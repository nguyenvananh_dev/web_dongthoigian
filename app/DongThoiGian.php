<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DongThoiGian extends Model
{
    public $tablename = 'dong_thoi_gian';

    public function User()
    {
        return beLongsTo('App\User', 'user_id');
    }
    public function UserTag()
    {
        return $this->hasMany('App\UserTag', 'dongthoigian_id');
    }
    public function Comment()
    {
        return $this->hasMany('App\Comment', 'dongthoigian_id');
    }
}
