<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    public $tablename = 'histories';

    public function User()
    {
        return beLongsTo('App\User', 'user_id');
    }
}
