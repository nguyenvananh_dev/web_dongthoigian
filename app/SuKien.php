<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuKien extends Model
{
    public $tablename = 'su_kien';

    public function User()
    {
        return beLongsTo('App\User', 'user_id');
    }
    public function BaiDang()
    {
        return $this->hasMany('App\BaiDang', 'sukien_id');
    }
    public function Comment()
    {
        return $this->hasMany('App\Comment', 'sukien_id');
    }
    public function UserTag()
    {
        return $this->hasMany('App\UserTag', 'sukien_id');
    }
}
