<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    const ACTIVE = 0;
    const BLOCK = 1;

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function DongThoiGian()
    {
        return $this->hasMany('App\DongThoiGian', 'user_id');
    }
    public function SuKien()
    {
        return $this->hasMany('App\SuKien', 'user_id');
    }
    public function BaiDang()
    {
        return $this->hasMany('App\BaiDang', 'user_id');
    }
    public function Comment()
    {
        return $this->hasMany('App\Comment', 'user_id');
    }
    public function UserTag()
    {
        return $this->hasMany('App\UserTag', 'user_id');
    }
    public static function getUser($key)
    {
        $user = false;
        if(! ($user = User::find($key)))
        {
            $user = User::where('username', changeUsername($key))->get();
            if(count($user) != 1) {
                $user = User::where('email', changeEmail($key))->get();
            }
            if(count($user) != 1) {
                $user = User::where('sodienthoai', changeNumberphone($key))->get();
            }
            if(count($user) == 1){
                $user = User::find($user[0]->id);
            }else{
                $user = false;
            }
        }
        if($user)
        {
            User::userDecode($user);
        }
        return $user;
    }
    public static function userDecode(&$user)
    {
        $user->username = rechangeUsername($user->username);
        $user->sodienthoai = rechangeNumberphone($user->sodienthoai);
        $user->email = rechangeEmail($user->email);
        $user->ngaysinh = rechangeDate($user->ngaysinh);
        return true;
    }
    public static function userEncode(&$user)
    {
        $user->username = changeUsername($user->username);
        $user->email = changeEmail($user->email);
        $user->sodienthoai = changeNumberphone($user->numberphone);
        $user->ngaysinh = changeDate($user->ngaysinh);
        return true;
    }
    public static function requestSelect($data, $select)
    {
        # Xử lý tìm kiếm thông tin
        $user = User::where('username', changeUsername($data->username))->get();
        if(count($user) != 1) {
            $user = User::where('email', changeEmail($data->username))->get();
        }
        if(count($user) != 1) {
            $user = User::where('sodienthoai', changeNumberphone($data->username))->get();
        }
        if(count($user) == 1 && $user[0]->status == User::ACTIVE){
            $user =  User::find($user[0]->id);
            $user->password = $data->password;
            return $user->only($select);
        }else{
            return [];
        }
    }
}
