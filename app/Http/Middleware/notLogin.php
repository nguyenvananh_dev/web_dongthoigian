<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class notLogin
{
    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
   public function handle($request, Closure $next)
   {
       try {
           JWTAuth::parseToken();
           JWTAuth::parseToken()->authenticate();
       }catch (JWTException $e) {
           if($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
            return $next($request);
           }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
            return $next($request);
           }else{
            return $next($request);
           }
       }
       return response()->json(['error'=>true, 'messeng' => 'Chức năng chỉ dành cho những người chưa đăng nhập']);
   }
}
