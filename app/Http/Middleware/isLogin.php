<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class isLogin
{
    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
   public function handle($request, Closure $next)
   {
       $er = false;
       try {
        JWTAuth::parseToken();
        JWTAuth::parseToken()->authenticate();
       }catch (JWTException $e) {
           if($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
               $er = true;
           }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
               $er = true;
           }else{
               $er = true;
           }
       }
       if($er){
           return response()->json(['error'=>true, 'messeng' => 'Chức năng chỉ dành cho những người đã đăng nhập']);
       }
       return $next($request);
   }
}
