<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Response;
use Validator;
use Auth;
use App\User;

class baseController extends ApiController
{
    public function getUser($key)
    {
        if($user_result = User::getUser($key)){
            return response()->json([
                'error' => false,
                'data' => $user_result
            ], 200);
        }

        return response()->json([
            'error' => true,
            'message' => 'Không tìm thấy người dùng'
        ], 404);
    }
	public function postRegister(Request $request)
	{
        ChangecomeBackALL($request);
        $request['usernameChange'] = changeUsername($request->username);
        if(($request['emailChange'] = changeEmail($request->email)) == null)
        {
            $request['emailChange'] = 'DEFAULT_VALUE';
        }
        if(($request['phoneChange'] = changeNumberphone($request->numberphone)) == null)
        {
            $request['phoneChange'] = 'DEFAULT_VALUE';
        }
		$rules = [
			'username' => 'required|min:3|max:50|alpha',
			'usernameChange' => 'unique:users,username',
			'name' => 'required',
			'email' => 'email',
			'emailChange' => 'unique:users,email',
			'phoneChange' => 'unique:users,sodienthoai',
			'password' => 'required|min:6|max:32',
			'password_again' => 'required|same:password'
		];
		$messengers = [
            'name.required' => 'Bạn chưa nhập Tên!',
            'username.required' => 'Bạn chưa nhập Tên tài khoản!',
            'username.min' => 'Tên tài khoản gồm tối thiểu 3 ký tự!',
            'username.max' => 'Tên tài khoản không được vượt quá 50 ký tự!',
            'username.alpha' => 'Tên tài khoản không được chứa ký tự đặc biệt!',
            'usernameChange.unique' => 'Tên tài khoản đã tồn tại!',
            'email.email' => 'Bạn chưa nhập đúng định dạng Email!',
            'emailChange.unique' => 'Địa chỉ Email đã tồn tại!',
            'phoneChange.unique' => 'Số điện thoại đã tồn tại!',
            'password.required' => 'Bạn chưa nhập mật khẩu!',
            'password.min' => 'Mật khẩu gồm tối thiểu 6 ký tự!',
            'password.max' => 'Mật khẩu không được vượt quá 32 ký tự!',
            'password_again.required' => 'Bạn chưa xác nhận mật khẩu!',
            'password_again.same' => 'Mật khẩu xác nhận chưa khớp với mật khẩu đã nhập!'
		];
		$validate = Validator::make($request->all(), $rules, $messengers);
		if($validate->fails()){
			return response()->json([
				'error' => true,
				'message' => $validate->errors()
			], 200);
		}else{
            $user = new User;
            $user->tenhienthi = $request->name;
            $user->username = ($request->username);
            $user->email = ($request->email);
            $user->sodienthoai = ($request->numberphone);
            $user->ngaysinh = ($request->ngaysinh);
            $user->password = bcrypt($request->password_again);
            $user->status = User::ACTIVE;
            $user->userEncode($user);
            $user->save();
            return response()->json([
                'error' => false,
                'message' => 'Đăng ký thành viên thành công.',
                'token' => JWTAuth::attempt(['username' => $user->username, 'password' => $request->password_again]),
                'user' => $user
            ], Response::HTTP_OK);
		}
	}
	public function postLogin(Request $request)
	{
        ChangecomeBackALL($request);
		$rules = [
			'username' => 'required',
			'password' => 'required|min:6|max:32',
		];
		$messengers = [
    			'username.required' => 'Bạn chưa nhập tài khoản đăng nhập!',
    			'password.required' => 'Bạn chưa nhập mật khẩu!',
    			'password.min' => 'Mật khẩu gồm tối thiểu 6 ký tự!',
    			'password.max' => 'Mật khẩu không được vượt quá 32 ký tự!',
		];
		$validate = Validator::make($request->all(), $rules, $messengers);
		if($validate->fails()){
			return response()->json([
				'error' => true,
				'message' => $validate->errors()
			], Response::HTTP_OK);
		}else{
            $credentials = User::requestSelect($request, ['username', 'password']);
			if(($token = JWTAuth::attempt($credentials))){
				# Đăng nhập thành công
				return response()->json([
					'error' => false,
                    'message' => 'Đăng nhập thành công!',
                    'token' => $token
				], Response::HTTP_OK);
			}else{
				#Đăng nhập thất bại
                return response()->json([
					'error' => true,
                ], Response::HTTP_BAD_REQUEST);
			}
		}
    }
    public function getLogout(Request $request)
    {
        Auth::logout($request->token);
        return response()->json([
            'error' => false,
            'messeng' => 'Đăng xuất thành công'
        ], Response::HTTP_BAD_REQUEST);
    }
    public function checkUserLogin(Request $request)
    {
        return response()->json([
            'error' => false,
            'user' => $this->ThisMe
        ], Response::HTTP_BAD_REQUEST);
    }
}
