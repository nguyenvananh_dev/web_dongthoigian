<?php

namespace App\Http\Controllers;

use App\Http\Controllers;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
class ApiController extends Controller
{
    protected $ThisMe = null;
    public function __construct(){
		$er = false;
		try {
			$parseToken = JWTAuth::parseToken();
			JWTAuth::parseToken()->authenticate();
		}catch (JWTException $e) {
			if($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
				$er = true;
			}else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
				$er = true;
			}else{
				$er = true;
			}
		}
		if(!$er){
			$user = JWTAuth::toUser($parseToken);
			User::userDecode($user);
			$this->ThisMe = $user;
		}
    }
}
