<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaiDang extends Model
{
    public $tablename = 'bai_dang';

    public function User()
    {
        return beLongsTo('App\User', 'user_id');
    }
    public function SuKien()
    {
        return beLongsTo('App\SuKien', 'sukien_id');
    }
    public function Comment()
    {
        return $this->hasMany('App\Comment', 'baidang_id');
    }
    public function UserTag()
    {
        return $this->hasMany('App\UserTag', 'baidang_id');
    }
}
