<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMixersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mixers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dongthoigian_id')->unsigned();
            $table->integer('sukien_id')->unsigned();
            $table->integer('danhso');
            $table->timestamps();
            $table->foreign('dongthoigian_id')->references('id')->on('dong_thoi_gian');
            $table->foreign('sukien_id')->references('id')->on('su_kien');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mixers');
    }
}
