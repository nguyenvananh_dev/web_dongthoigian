<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type');
            $table->integer('user_id')->unsigned();
            $table->integer('dongthoigian_id')->nullable()->unsigned();
            $table->integer('sukien_id')->nullable()->unsigned();
            $table->integer('baidang_id')->nullable()->unsigned();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('dongthoigian_id')->references('id')->on('dong_thoi_gian');
            $table->foreign('sukien_id')->references('id')->on('su_kien');
            $table->foreign('baidang_id')->references('id')->on('bai_dang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_tags');
    }
}
